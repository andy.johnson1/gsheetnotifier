/**
 * A special function that runs when the spreadsheet is first
 * opened or reloaded. onOpen() is used to add custom menu
 * items to the spreadsheet.
 */
function onOpen() {
  var ui = SpreadsheetApp.getUi();
  menu = ui.createMenu('Reminder');
  menu.addItem('Send Reminders', 'sendReminders2').addToUi();
  menu.addItem("Update Properties", "showSidebar").addToUi();
}

/**** begin metadata adder ****/

function showAlert(key) {
  var ui = SpreadsheetApp.getUi(); // Same variations.
  var message = 'Are you sure you want to delete the key : {key}?';
  message = message.replace('{key}',key);
  var result = ui.alert(
     'Please confirm',
     message,
      ui.ButtonSet.YES_NO);

  // Process the user's response.
  if (result == ui.Button.YES) {
    // User clicked "Yes".
    deleteCustomProperty(key);
  } else {
    // User clicked "No" or X in the title bar.
    return 0;
  }
}



function findCustomPropertiesFromResult(data) {
    var foundData={};
    var keys = data.getKeys();
    if(keys.length >0) {
      foundData['status'] = 1;
      foundData['data'] = [];
      var temp = {};
      for (var i = 0; i < keys.length; i++) {
        temp = {};
        temp['key'] = keys[i];
        temp['value'] = data.getProperty(keys[i]);
        foundData['data'].push(temp)
      }
      return foundData;
    }
    else {
      return {status:0};
    }
}

function getCustomProperties() {
  var propertyData = findCustomPropertiesFromResult(PropertiesService.getDocumentProperties());
  return propertyData;
}


function deleteCustomProperty(key) {
  PropertiesService.getDocumentProperties().deleteProperty(key);
  return 1;
}

function updateCustomProperty(key,value) {
  var docProps = PropertiesService.getDocumentProperties();
  docProps.setProperty(key,value);
}


function addCustomProperty(key,value) {
  PropertiesService.getDocumentProperties().setProperty(key,value);
}


/* Show a 300px sidebar with the HTML from googlemaps.html */
function showSidebar() {
  var html = HtmlService.createTemplateFromFile("metadata_adder")
    .evaluate()
    .setTitle("Document Properties"); // The title shows in the sidebar
  SpreadsheetApp.getUi().showSidebar(html);
}
/**** end metadata adder ****/

var EMAIL_SENT = 'EMAIL_SENT';

Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

function sendReminders2() { 
  // Get the sheet where the data is, in sheet 'system'
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("Sheet1");
  var dataRange = sheet.getDataRange();
  // Fetch values for each row in the Range to input into the mailing system 
  var data = dataRange.getValues(); 
  // consider putting a loop defining column indexes by column labels here, to make reading/updating easier
  // e.g.: for col in data[0]
  var i = 0;
  for (i in data) { 
    var row = data[i]; 
    try {
      var dueDate = Utilities.formatDate(row[6], "GMT", "MM/dd/yyyy");
    } 
    catch (err) {
      continue;
    }
    var emailAddress = row[5];
    var reqId = row[0];
    var reqName = row[3];
    var reqDesc = row[4];
    var todaysDate = Utilities.formatDate(new Date(), "GMT", "MM/dd/yyyy");
    var todaysDatePlus7 = Utilities.formatDate(new Date().addDays(7), "GMT", "MM/dd/yyyy");
    var todaysDatePlus30 = Utilities.formatDate(new Date().addDays(30), "GMT", "MM/dd/yyyy");
    var todaysDatePlus90 = Utilities.formatDate(new Date().addDays(90), "GMT", "MM/dd/yyyy");
    var logEntry = "";
   

    switch (dueDate) {
      case (todaysDate): // if duedate is TODAY
        // We found a match, lets check if the email was already sent today
        logEntry = EMAIL_SENT + " " + todaysDate + " for expiry";
        if (row[7].match(logEntry) != logEntry) {
          // Email not yet sent, but due date is here let's do this thing.
          var subject = "Your Request " + reqId + " is due TODAY!";
          var message = subject + "\r\n" + "Request ID: " + reqId + "\r\nReqName: " + reqName + "\r\nReqDesc: " + reqDesc + "\r\n";
          MailApp.sendEmail(emailAddress, subject, message);
          //sheet.getRange(parseInt(i)+1, 8).setValue(EMAIL_SENT + " " + todaysDate);
          logEntry = sheet.getRange(parseInt(i)+1, 8).getValue() + "\n" + logEntry;
          sheet.getRange(parseInt(i)+1, 8).setValue(logEntry);
          
          // Make sure the cell is updated right away in case the script is interrupted
          SpreadsheetApp.flush();
        }
        else {
          Logger.log("Already sent 0d reminder for: " + reqId)
        }
        break;
      case (todaysDatePlus7): // if duedate is a week from now
        // We found a match, lets check if the email was already sent
        logEntry = EMAIL_SENT + " " + todaysDatePlus7 + " for 7d warning";
        if (row[7].match(EMAIL_SENT + " " + todaysDatePlus7) != EMAIL_SENT + " " + todaysDatePlus7) {
          // Email not yet sent, but due date is here let's do this thing.
          var subject = "Your Request " + reqId + " is due IN 7 DAYS!";
          var message = subject + "\r\n" + "Request ID: " + reqId + "\r\nReqName: " + reqName + "\r\nReqDesc: " + reqDesc + "\r\n";
          MailApp.sendEmail(emailAddress, subject, message);
          logEntry = sheet.getRange(parseInt(i)+1, 8).getValue() + "\n" + logEntry;
          sheet.getRange(parseInt(i)+1, 8).setValue(logEntry);
          // Make sure the cell is updated right away in case the script is interrupted
          SpreadsheetApp.flush();
        }
        else {
          Logger.log("Already sent 7d reminder for: " + reqId)
        }
        break;
      case (todaysDatePlus30): // if duedate is 30d from now
        // We found a match, lets check if the email was already sent
        logEntry = EMAIL_SENT + " " + todaysDatePlus30 + " for 30d warning";
        if (row[7].match(EMAIL_SENT) != EMAIL_SENT) {
          // Email not yet sent, but due date is here let's do this thing.
          var subject = "Your Request " + reqId + " is due IN 30 DAYS!";
          var message = subject + "\r\n" + "Request ID: " + reqId + "\r\nReqName: " + reqName + "\r\nReqDesc: " + reqDesc + "\r\n";
          MailApp.sendEmail(emailAddress, subject, message);
          logEntry = sheet.getRange(parseInt(i)+1, 8).getValue() + "\n" + logEntry;
          sheet.getRange(parseInt(i)+1, 8).setValue(logEntry);
          // Make sure the cell is updated right away in case the script is interrupted
          SpreadsheetApp.flush();
        }
        else {
          Logger.log("Already sent 30d reminder for: " + reqId)
        }
        break;
      case (todaysDatePlus90): // if duedate is 90d from now
        // We found a match, lets check if the email was already sent
        logEntry = EMAIL_SENT + " " + todaysDatePlus90 + " for 90d warning";
        if (row[7].match(EMAIL_SENT) != EMAIL_SENT) {
          // Email not yet sent, but due date is here let's do this thing.
          var subject = "Your Request " + reqId + " is due IN 90 DAYS!";
          var message = subject + "\r\n" + "Request ID: " + reqId + "\r\nReqName: " + reqName + "\r\nReqDesc: " + reqDesc + "\r\n";
          MailApp.sendEmail(emailAddress, subject, message);
          logEntry = sheet.getRange(parseInt(i)+1, 8).getValue() + "\n" + logEntry;
          sheet.getRange(parseInt(i)+1, 8).setValue(logEntry);
          // Make sure the cell is updated right away in case the script is interrupted
          SpreadsheetApp.flush();
        }
        else {
          Logger.log("Already sent 90d reminder for: " + reqId)
        }
        break;
      default:
        Logger.log("No due date milestones for: " + reqId);
    }
  } 
}